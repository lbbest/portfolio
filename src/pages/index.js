import React from "react"

import LinkButton from "../components/shared/linkbutton"
import ProjectCard from "../components/shared/projectcard"
import Image from "../components/image"

import Layout from "../components/layout"
import SEO from "../components/seo"

import "../scss/styles.scss"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <div class="index container-fluid p-0">
      <div class="index__hero">
        <div class="index__hero__inner container">
          <div class="row">
            <div class="col">
              <h1>Hi there!</h1>
              <h1>I'm Liam, a full-stack web developer and designer.</h1>
              <p>
                I build blazing-fast static websites and powerful web
                applications.
              </p>
              <LinkButton link="contact" text="Get in touch" />
            </div>
          </div>
        </div>
      </div>
      <div class="index__projects">
        <div class="index__projects__inner container">
          <div class="row">
            <div class="col">
              <h2 class="index__projects__inner__title">Latest Projects</h2>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <div class="index__projects__inner__container">
                <ProjectCard
                  image="brandtrax.png"
                  title="brandtrax"
                  text="A full-stack professional web application for music advertising."
                  small="Vue, Sass, Laravel, MySQL"
                  link="https://brandtrax.io/"
                />
                <ProjectCard
                  image="ledomaine.png"
                  title="Le Domaine"
                  text="Promotional website for a holiday property in France."
                  small="Gatsby, Sass, Bootstrap"
                  link="http://residenceledomaine.com/"
                />
                <ProjectCard
                  image="musicall.png"
                  title="music.all"
                  text="A front-end music discovery and information service."
                  small="React, Spotify API, Ticketmaster API"
                  link="https://musicallv1.netlify.app/"
                />
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <LinkButton link="projects" text="View all" />
            </div>
          </div>
        </div>
      </div>
      <div class="index__services">
        <div class="index__services__inner container">
          <div class="row">
            <div class="index__services__inner__service">
              <div class="col">
                <div class="index__services__inner__service__image">
                  <Image filename="code_1.png" alt="code" />
                </div>
              </div>
              <div class="col-8">
                <div class="index__services__inner__service__desc">
                  <h2 class="index__services__inner__service__desc__title">
                    Web Development
                  </h2>
                  <p class="index__services__inner__service__desc__text">
                    <br />
                    I offer full-stack web development using a range of front-
                    and back-end technologies to create extremely quick static
                    websites for businesses as well as powerful interactive web
                    applications.
                    <br />
                    <br />
                    From informational pages to blogs and e-commerce platforms,
                    I make use of cutting-edge modern technologies that fit the
                    exact uses and needs of your website from a range of tech
                    including React, Vue, Gatsby, Laravel/PHP and MySQL.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="index__services__inner__service">
              <div class="col">
                <div class="index__services__inner__service__image">
                  <Image filename="design_1.png" alt="design" />
                </div>
              </div>
              <div class="col-8">
                <div class="index__services__inner__service__desc">
                  <h2 class="index__services__inner__service__desc__title">
                    Web Design
                  </h2>
                  <p class="index__services__inner__service__desc__text">
                    <br />
                    It's important that your website also looks great.
                    Development and design are highly interconnected and I
                    always make sure that your website looks good, clean and
                    accessible for its users using tech such as Sass and
                    Bootstrap.
                    <br />
                    <br />I also offer graphic design for company branding
                    needs, asset editing to ensure your content is ready for use
                    and of high-quality as well as web design services to create
                    concepts and wireframes for your website.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="index__services__inner__service">
              <div class="col">
                <div class="index__services__inner__service__image">
                  <Image filename="deploy_1.png" alt="deploy" />
                </div>
              </div>
              <div class="col-8">
                <div class="index__services__inner__service__desc">
                  <h2 class="index__services__inner__service__desc__title">
                    Website Deployment and Hosting
                  </h2>
                  <p class="index__services__inner__service__desc__text">
                    <br />
                    Once you have your website you'll need to release it into
                    the wild and keep it maintained. I can deal with domain
                    management in order host your website and deploy your
                    completed product to the world wide web.
                    <br />
                    <br />
                    Forget about the worries of buying and renewing a domain and
                    keeping your deployed website up-to-date - I'll handle it.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="index__questions">
        <div class="index__questions__inner container">
          <p class="index__questions__inner__mark">?</p>
          <div class="index__questions__inner__desc">
            <h2 class="index__questions__inner__desc__title">Questions?</h2>
            <p class="index__questions__inner__desc__text">
              If you have any questions or would like some more information feel
              free to get in contact with me via my contact form and I'll get
              back to you as soon as possible!
            </p>
            <LinkButton link="/contact" text="Get in contact" />
          </div>
        </div>
      </div>
    </div>
  </Layout>
)

export default IndexPage
