import React from "react"

import ProjectCard from "../components/shared/projectcard"

import Layout from "../components/layout"
import SEO from "../components/seo"

const ProjectsPage = () => (
  <Layout>
    <SEO title="Projects" />
    <div class="projects container-fluid p-0">
      <div class="projects__intro">
        <div class="projects__intro__inner container">
          <h2 class="projects__intro__inner__title">Projects</h2>
          <p class="projects__intro__inner__text"></p>
        </div>
      </div>
      <div class="projects__main">
        <div class="projects__main__inner container">
          <div class="projects__main__inner__container">
            <ProjectCard
              image="brandtrax.png"
              title="brandtrax"
              text="A full-stack professional web application for music advertising."
              small="Vue, Sass, Laravel, MySQL"
              link="https://brandtrax.io/"
            />
            <ProjectCard
              image="ledomaine.png"
              title="Le Domaine"
              text="Promotional website for a holiday property in France."
              small="Gatsby, Sass, Bootstrap"
              link="http://residenceledomaine.com/"
            />
            <ProjectCard
              image="musicall.png"
              title="music.all"
              text="A front-end music discovery and information service."
              small="React, Spotify API, Ticketmaster API"
              link="https://musicallv1.netlify.app/"
            />
            <ProjectCard
              image="ficticiousmarket.png"
              title="FicticiousMarket"
              text="An e-commerce page with fully-functional cart and search features."
              small="React, Redux"
              link="https://stoic-elion-5c0aa4.netlify.app/"
            />
            <ProjectCard
              image="tgif.png"
              title="TGIF"
              text="A informational website related to the 113th US Congress."
              small="HTML, CSS, Vanilla JS, API"
              link="https://quirky-almeida-2da557.netlify.app/"
            />
            <ProjectCard
              image="interactivetable.png"
              title="Interactive Table"
              text="A fully interactive table with draggable columns and renaming."
              small="React, DraggableColumns"
              link="https://peaceful-knuth-30cd52.netlify.app/"
            />
            <ProjectCard
              image="liambest.png"
              title="Portfolio"
              text="This website! A simple website to show my current and past projects."
              small="Gatsby, Sass, Bootstrap"
              link="https://www.liambest.co.uk/"
            />
            <div class="projectcard" style={{ visibility: "hidden" }}></div>
            <div class="projectcard" style={{ visibility: "hidden" }}></div>
          </div>
        </div>
      </div>
    </div>
  </Layout>
)

export default ProjectsPage
