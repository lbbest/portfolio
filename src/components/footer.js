import React from "react"
import { Link } from "gatsby"
import Image from "./image"

const Footer = () => {
  return (
    <div class="footer container-fluid">
      <div class="footer__inner container">
        <div class="footer__inner__links">
          <Link
            class="footer__inner__links__link"
            to="/about"
            activeClassName="activefooter"
          >
            About
          </Link>
          <Link
            class="footer__inner__links__link"
            to="/projects"
            activeClassName="activefooter"
          >
            Projects
          </Link>
          <Link
            class="footer__inner__links__link"
            to="/contact"
            activeClassName="activefooter"
          >
            Contact
          </Link>
        </div>
        <div class="footer__inner__middle">
          <h2 class="footer__inner__middle__logo">&lt;liambest&gt;</h2>
          <p class="footer__inner__middle__copyright">&copy; Liam Best 2020</p>
        </div>
        <div class="footer__inner__social">
          <a
            class="footer__inner__social__logo"
            href="https://gitlab.com/lbbest"
            target="_blank"
            rel="noreferrer"
          >
            <Image filename="gitlab.png" alt="gitlab" />
          </a>
          <a
            class="footer__inner__social__logo"
            href="https://www.linkedin.com/in/liam-best-816355b0/"
            target="_blank"
            rel="noreferrer"
          >
            <Image filename="linkedin.png" alt="linkedin" />
          </a>
        </div>
      </div>
    </div>
  )
}

export default Footer
