import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"

import "../scss/styles.scss"

const Nav = () => (
  <div class="navcontainer container-fluid">
    <nav class="nav container">
      <Link class="nav__home" activeClassName="activehome" to="/">
        &lt;liambest&gt;
      </Link>
      <div class="nav__list">
        <Link class="nav__list__link" to="/about/" activeClassName="active">
          About
        </Link>
        <Link class="nav__list__link" to="/projects/" activeClassName="active">
          Projects
        </Link>
        <Link class="nav__list__link" to="/contact/" activeClassName="active">
          Contact
        </Link>
      </div>
    </nav>
  </div>
)

Nav.propTypes = {
  siteTitle: PropTypes.string,
}

Nav.defaultProps = {
  siteTitle: ``,
}

export default Nav
